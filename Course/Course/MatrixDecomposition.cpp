#include "MatrixDecomposition.h"
#include <math.h>
#include <list>
#include <iostream>
#include <omp.h>

using namespace std;

class MatrixDecomposition {
	// �������� ������� ���� � ������� LDU - ����������
public:
	static double* LDU(SystemOfLinearEquations SLE, int n)
	{
		SLE.LDU = Result();
		double** A = new double* [n];
		double* B = new double[n];
		double* D = new double[n];
		double** L = new double* [n];
		for (int i = 0; i < n; i++)
		{
			L[i] = new double[n];
			A[i] = new double[n];
		}
		CopyArray(SLE.A, SLE.B, A, B, n);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				L[i][j] = 0;

		double** U = new double* [n];
		for (int i = 0; i < n; i++) U[i] = new double[n];

		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				U[i][j] = A[i][j];

		int i, j, k;
		double sum;
		for (int i = 0; i < n; i++)
			for (int j = i; j < n; j++)
				L[j][i] = U[j][i] / U[i][i];

		for (int k = 0; k < n; k++)
		{
			for (int i = k; i < n; i++)
				for (int j = i; j < n; j++)
					L[j][i] = U[j][i] / U[i][i];

			for (int i = k + 1; i < n; i++)
				for (int j = k; j < n; j++)
					U[i][j] = U[i][j] - L[i][k] * U[k][j];

			D[k] = U[k][k];
			for (int j = k; j < n; j++)
			{
				U[k][j] = U[k][j] / D[k];
			}
		}
		double* Z = new double[n];
		for (i = 0; i < n; i++)
		{
			sum = 0;
			for (j = 0; j < i; j++)
				sum += L[i][j] * Z[j];
			Z[i] = B[i] - sum;
		}
		double* Y = new double[n];
		for (i = 0; i < n; i++)
		{
			Y[i] = Z[i] / D[i];
		}
		double* X = new double[n];
		for (int i = 0; i < n; i++)
			X[i] = 0;
		for (i = n - 1; i >= 0; i--)
		{
			sum = 0;
			for (j = i; j < n; j++)
				sum += U[i][j] * X[j];
			X[i] = Y[i] - sum;
		}
		SLE.LDU.X = X;
		return X;
	}
	 
	//// ������������ ������� ���� LDU
	static double* LDUParallel(SystemOfLinearEquations SLE, int n, int numberThreads) {
		SLE.LDUParallel = Result();
		double** A = new double* [n];
		double* B = new double[n];
		double* D = new double[n];
		double** L = new double* [n];
		for (int i = 0; i < n; i++)
		{
			L[i] = new double[n];
			A[i] = new double[n];
		}

		CopyArray(SLE.A,SLE.B,A,B,n);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				L[i][j] = 0;
		omp_set_num_threads(numberThreads);
		double** U = new double* [n];
		for (int i = 0; i < n; i++) U[i] = new double[n];
#pragma omp parallel for
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				U[i][j] = A[i][j];

		int i, j, k;
		double sum;
		for (int i = 0; i < n; i++)
#pragma omp parallel for private(j)
			for (int j = i; j < n; j++)
				L[j][i] = U[j][i] / U[i][i];

		for (int k = 0; k < n; k++)
		{
			for (int i = k; i < n; i++)
				for (int j = i; j < n; j++)
					L[j][i] = U[j][i] / U[i][i];
#pragma omp parallel for shared(U,n,k) private(i) schedule(static, 64)
			for (int i = k + 1; i < n; i++)
				for (int j = k; j < n; j++)
					U[i][j] = U[i][j] - L[i][k] * U[k][j];

			D[k] = U[k][k];
			for (int j = k; j < n; j++)
			{
				U[k][j] = U[k][j] / D[k];
			}
		}
		double* Z = new double[n];
		for (i = 0; i < n; i++)
		{
			sum = 0;
#pragma omp parallel for private(j)
			for (j = 0; j < i; j++)
				sum += L[i][j] * Z[j];
			Z[i] = B[i] - sum;
		}
		double* Y = new double[n];
#pragma omp parallel for private(i,n)
		for (i = 0; i < n; i++)
		{
			Y[i] = Z[i] / D[i];
		}
		double* X = new double[n];
		for (int i = 0; i < n; i++)
			X[i] = 0;

		for (i = n - 1; i >= 0; i--)
		{
			sum = 0;
#pragma omp parallel for private(j,n)
			for (j = i; j < n; j++)
				sum += U[i][j] * X[j];
			X[i] = Y[i] - sum;
		}
		SLE.LDUParallel.X = X;
		return X;
	}
	static double* GaussianEliminationParallel(SystemOfLinearEquations SLE, int n, int numberThreads)
	{

		double** a = new double* [n];
		for (int i = 0; i < n; i++) a[i] = new double[n];
		double* y = new double[n];
		CopyArray(SLE.A, SLE.B, a, y, n);
		double* x, max;
		int k, index;
		const double eps = 0.00001;
		x = new double[n];
		k = 0;
		while (k < n)
		{
			max = abs(a[k][k]);
			index = k;
			for (int i = k + 1; i < n; i++)
			{
				if (abs(a[i][k]) > max)
				{
					max = abs(a[i][k]);
					index = i;
				}
			}
			if (max < eps)
			{
				// ��� ��������� ������������ ���������

				cout << "������� �������� ���������� ��-�� �������� ������� ";
				cout << index << " ������� A" << endl;
				return 0;
			}
#pragma omp parallel for private(i,n)
			for (int j = 0; j < n; j++)
			{
				double temp = a[k][j];
				a[k][j] = a[index][j];
				a[index][j] = temp;
			}
			double temp = y[k];
			y[k] = y[index];
			y[index] = temp;
			// ������������ ���������
#pragma omp parallel for private(i,n,k)
			for (int i = k; i < n; i++)
			{
				double temp = a[i][k];
				if (abs(temp) < eps) continue; // ��� �������� ������������ ����������
				for (int j = 0; j < n; j++)
					a[i][j] = a[i][j] / temp;
				y[i] = y[i] / temp;
				if (i == k)  continue; // ��������� �� �������� ���� �� ����
#pragma omp parallel for private(j)
				for (int j = 0; j < n; j++)
					a[i][j] = a[i][j] - a[k][j];
				y[i] = y[i] - y[k];
			}
			k++;
		}
		// �������� �����������
		#pragma omp parallel for private(i,n,k)
		for (k = n - 1; k >= 0; k--)
		{
			x[k] = y[k];
			for (int i = 0; i < k; i++)
				y[i] = y[i] - a[i][k] * x[k];
		}
		for (int i = 0; i < n; i++) {
			x[i] = floor(x[i] * 100) / 100;

		}
		SLE.GaussianElimination.X = x;
		return x;
	}
	static double* GaussianElimination(SystemOfLinearEquations SLE, int n)
	{
		
		double** a = new double* [n];
		for (int i = 0; i < n; i++) a[i] = new double[n];
		double* y = new double[n];
		CopyArray(SLE.A, SLE.B, a, y, n);
		double* x, max;
		int k, index;
		const double eps = 0.00001; 
		x = new double[n];
		k = 0;
		while (k < n)
		{
			max = abs(a[k][k]);
			index = k;
			for (int i = k + 1; i < n; i++)
			{
				if (abs(a[i][k]) > max)
				{
					max = abs(a[i][k]);
					index = i;
				}
			}
			// ������������ �����
			if (max < eps)
			{
				// ��� ��������� ������������ ���������

				cout << "������� �������� ���������� ��-�� �������� ������� ";
				cout << index << " ������� A" << endl;
				return 0;
			}
			for (int j = 0; j < n; j++)
			{
				double temp = a[k][j];
				a[k][j] = a[index][j];
				a[index][j] = temp;
			}
			double temp = y[k];
			y[k] = y[index];
			y[index] = temp;
			// ������������ ���������
			for (int i = k; i < n; i++)
			{
				double temp = a[i][k];
				if (abs(temp) < eps) continue; // ��� �������� ������������ ����������
				for (int j = 0; j < n; j++)
					a[i][j] = a[i][j] / temp;
				y[i] = y[i] / temp;
				if (i == k)  continue; // ��������� �� �������� ���� �� ����
				for (int j = 0; j < n; j++)
					a[i][j] = a[i][j] - a[k][j];
				y[i] = y[i] - y[k];
			}
			k++;
		}
		// �������� �����������
		for (k = n - 1; k >= 0; k--)
		{
			x[k] = y[k];
			for (int i = 0; i < k; i++)
				y[i] = y[i] - a[i][k] * x[k];
		}
		for (int i = 0; i < n; i++) {
			x[i] = floor(x[i] * 100) / 100;

		}
		SLE.GaussianElimination.X = x;
		return x;
	}
	static void CopyArray(double** A, double* B, double** newA, double* newB,int n) {
		for (int i = 0; i < n;i++) {
			for (int j = 0; j< n;j++)
			{
				newA[i][j] = A[i][j];
			}
			newB[i] = B[i];
		}
	}
};