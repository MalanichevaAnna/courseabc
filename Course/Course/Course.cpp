﻿// Course.cpp : Определяет точку входа для приложения.
//

#include "framework.h"
#include "Course.h"
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <OleCtl.h> 
#include <tchar.h> 
#include "atlstr.h"
#include <tlhelp32.h>
#include <iostream>
#include <regex>
#include <Windows.h>
#include <ctime>
#include <omp.h>
#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
bool CheckArray(SystemOfLinearEquations SLE, int size);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Разместите код здесь.

    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_COURSE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_COURSE));

    MSG msg;

    // Цикл основного сообщения:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_COURSE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)COLOR_BTNSHADOW;//(HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_COURSE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0,660,500, NULL,NULL, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//


static SystemOfLinearEquations SLE;// = new SystemOfLinearEquations();
double* x;
double* x1;
double* x2;
double* x3;
static int n;
static int numberThreads;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//flush(stdin);
	/*double** array_ = new double*[3];
	for (int i = 0; i < n; i++) array_[i] = new double[3];*/

	LPWSTR buf = NULL;
	int i;
	int j;
	static HWND hTextBox;
	static HWND hWndListX, hWndListX1, hWndListX2, hWndListX3, timeListLDU, timeListGaus,timeListLDUParallel, timeListGParallel, hTextThreads;
	switch (message)
	{

	case WM_CREATE:
	{
		setlocale(LC_ALL, "Russian");
		
		//hWndList = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER/*| LBS_SORT | WS_VSCROLL | ES_AUTOVSCROLL*/, 250, 20, 250, 330, hWnd, (HMENU)3, NULL, NULL);
		HWND button = CreateWindowExW(WS_EX_TRANSPARENT, L"BUTTON", L"Read matrix from file",
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			26, 20, 150, 40,
			hWnd, (HMENU)1,
			GetModuleHandle(NULL), 0);
		HWND buttonResult = CreateWindowExW(WS_EX_TRANSPARENT, L"BUTTON", L"Result",
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			26, 80, 150, 40,
			hWnd, (HMENU)2,
			GetModuleHandle(NULL), 0);
		HWND buttonRundom = CreateWindowExW(WS_EX_TRANSPARENT, L"BUTTON", L"Rundom",
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			26, 140, 150, 40,
			hWnd, (HMENU)3,
			GetModuleHandle(NULL), 0);

		hTextBox = CreateWindowEx(NULL, TEXT("edit"), TEXT("300"),
			WS_VISIBLE | WS_CHILD | WS_BORDER,
			185, 140, 60, 38, hWnd, (HMENU)4,NULL, NULL);

		hTextThreads = CreateWindowEx(NULL, TEXT("edit"), TEXT("100"),
			WS_VISIBLE | WS_CHILD | WS_BORDER,
			185, 200, 60, 38, hWnd, (HMENU)4, NULL, NULL);

		HWND textThreads = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("Number of threads"),
			WS_CHILD | WS_VISIBLE,
			26, 210, 150, 20,
			hWnd, NULL, NULL, NULL);

		HWND text = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("LDU"),
			WS_CHILD | WS_VISIBLE,
			250, 5, 80, 20, hWnd, NULL, NULL, NULL);
		hWndListX = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			250, 20, 80, 330, hWnd, (HMENU)5, NULL, NULL);
		HWND text1 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("Gauss"),
			WS_CHILD | WS_VISIBLE,
			340, 5, 80, 20, hWnd, NULL, NULL, NULL);
		hWndListX1 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			340, 20, 80, 330, hWnd, (HMENU)5, NULL, NULL);
		HWND text2 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("LDUP"),
			WS_CHILD | WS_VISIBLE,
			430, 5, 80, 20, hWnd, NULL, NULL, NULL);
		hWndListX2 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			430, 20, 80, 330, hWnd, (HMENU)5, NULL, NULL);
		HWND text3 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("GaussP"),
			WS_CHILD | WS_VISIBLE,
			520, 5, 80, 20, hWnd, NULL, NULL, NULL);
		hWndListX3 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			520, 20, 80, 330, hWnd, (HMENU)5, NULL, NULL);
		HWND timeText = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("RunTime:"),
			WS_CHILD | WS_VISIBLE,
			250, 350, 80, 20, hWnd, NULL, NULL, NULL);
		timeListLDU = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			250, 370, 80, 20, hWnd, (HMENU)5, NULL, NULL);
		HWND timeText1 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("RunTime:"),
			WS_CHILD | WS_VISIBLE,
			340, 350, 80, 20, hWnd, NULL, NULL, NULL);
		timeListGaus = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			340, 370, 80, 20, hWnd, (HMENU)5, NULL, NULL);
		HWND timeText2 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("RunTime:"),
			WS_CHILD | WS_VISIBLE,
			430, 350, 80, 20, hWnd, NULL, NULL, NULL);
		    timeListLDUParallel = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			430, 370, 80, 20, hWnd, (HMENU)5, NULL, NULL);
		HWND timeText3 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("static"), TEXT("RunTime:"),
			WS_CHILD | WS_VISIBLE,
			520, 350, 80, 20, hWnd, NULL, NULL, NULL);
		    timeListGParallel = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("listbox"), TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL,
			520, 370, 80, 20, hWnd, (HMENU)5, NULL, NULL);

		//SendMessage(hWndList, LB_SETITEMDATA, i, (LPARAM)13);
	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Разобрать выбор в меню:
		switch (wmId)
		{
		case 1:
		{
			SendMessage(hWndListX, LB_RESETCONTENT, 0, 0);
			SendMessage(hWndListX1, LB_RESETCONTENT, 0, 0);
			SendMessage(hWndListX3, LB_RESETCONTENT, 0, 0);
			SendMessage(hWndListX2, LB_RESETCONTENT, 0, 0);
			SendMessage(timeListLDU, LB_RESETCONTENT, 0, 0);
			SendMessage(timeListGaus, LB_RESETCONTENT, 0, 0);
			SendMessage(timeListGParallel, LB_RESETCONTENT, 0, 0);
			SendMessage(timeListLDUParallel, LB_RESETCONTENT, 0, 0);
			ifstream in("input.txt");
			ifstream inB("inputB.txt");
			if (in.is_open())
			{
				int count = 0;
				int temp;
				while (!in.eof())
				{
					in >> temp;
					count++;
				}

				in.seekg(0, ios::beg);
				in.clear();

				int count_space = 0;
				char symbol;
				while (!in.eof())
				{
					in.get(symbol);
					if (symbol == ' ') count_space++;
					if (symbol == '\n') break;
				}

				in.seekg(0, ios::beg);
				in.clear();

				n = count / (count_space + 1);
				int m = count_space + 1;
				SLE.A = new double* [n];
				for (int i = 0; i < n; i++) SLE.A[i] = new double[m];

				for (int i = 0; i < n; i++)
					for (int j = 0; j < m; j++)
					{
						in >> temp;
						SLE.A[i][j] = temp;
					}

				for (int i = 0; i < n; i++)
				{
					for (int j = 0; j < m; j++)
					{			
						temp = SLE.A[i][j];			
					}			
				}

				in.close();
				
			}
			if (inB.is_open())
			{
				int count = 0;// число чисел в файле
				int temp;//Временная переменная

				while (!inB.eof())// пробегаем пока не встретим конец файла eof
				{
					inB >> temp;//в пустоту считываем из файла числа
					count++;// увеличиваем счетчик числа чисел
				}

				inB.seekg(0, ios::beg);
				inB.clear();

				//Число пробелов в первой строчке вначале равно 0
				int count_space = 0;
				char symbol;
				while (!in.eof())//на всякий случай цикл ограничиваем концом файла
				{
					//теперь нам нужно считывать не числа, а посимвольно считывать данные
					in.get(symbol);//считали текущий символ
					if (symbol == ' ') count_space++;//Если это пробел, то число пробелов увеличиваем
					if (symbol == '\n') break;//Если дошли до конца строки, то выходим из цикла
				}

				in.seekg(0, ios::beg);
				in.clear();

				n = count / (count_space + 1);//число строк
				//int m = count_space + 1;//число столбцов на единицу больше числа пробелов
				SLE.B = new double[n];
				//	for (int i = 0; i < n; i++) x[i] = new double[m];

				for (int i = 0; i < n; i++)
				{
					in >> temp;
					SLE.B[i] = temp;
				}

				for (int i = 0; i < n; i++)
				{
					//cout << x[i][j] << "\t";'
					temp = SLE.B[i];
					//SendMessage(hWndList, LB_ADDSTRING, 0, (LPARAM)1);	
				//cout << "\n";
				}

				//for (int i = 0; i < n; i++) delete[] x[i];
				//delete[] x;
				inB.close();//под конец закроем файла
				if (!CheckArray(SLE, n))
				{
					MessageBox(hWnd, L"Error.", L"Information", NULL);
					SLE.A = NULL;
					SLE.B = NULL;
					break;
				}
				MessageBox(hWnd, L"Data received.", L"Information", NULL);
			}
			else
			{
				//Если открытие файла прошло не успешно
				MessageBox(hWnd, L"File not found.", L"Information",NULL);
				//return 0;
			}
		}
		break;

		case 3:
		{

			TCHAR* buf = new TCHAR[100];
			GetWindowText(hTextBox, buf, 100);
			n = _ttoi(buf);
			TCHAR* buf1 = new TCHAR[100];
			GetWindowText(hTextThreads, buf1, 100);
			numberThreads = _ttoi(buf1);
			if (n != 0 && numberThreads !=0)
			{
				SLE.A = new double* [n];
				for (int i = 0; i < n; i++) SLE.A[i] = new double[n];

				SLE.B = new double[n];

				for (int i = 0; i < n; i++)
				{
					for (int j = 0; j < n; j++)
					{
						SLE.A[i][j] = 0;
					}
					SLE.B[i] = 0;
				}
				double a, b;
				for (int i = 0; i < n; i++)
				{
					for (int j = 0; j < n; j++)
					{
						SLE.A[i][j] = rand() % 30 + 0;
						a = SLE.A[i][j];


					}
					SLE.B[i] = rand() % 30 + 0;
					b = SLE.B[i];
				}
				MessageBox(hWnd, L"Matrix ", L"Information", NULL);
			}
			else {
				MessageBox(hWnd, L"INCORRECT Data ", L"Information", NULL);
			}
		    
		}
		break;
		case 2:
		{

			if (SLE.A != NULL && SLE.B != NULL && CheckArray(SLE,n))
			{
				SendMessage(hWndListX, LB_RESETCONTENT, 0, 0);
				SendMessage(hWndListX1, LB_RESETCONTENT, 0, 0);
				SendMessage(hWndListX3, LB_RESETCONTENT, 0, 0);
				SendMessage(hWndListX2, LB_RESETCONTENT, 0, 0);
				SendMessage(timeListLDU, LB_RESETCONTENT, 0, 0);
				SendMessage(timeListGaus, LB_RESETCONTENT, 0, 0);
				SendMessage(timeListGParallel, LB_RESETCONTENT, 0, 0);
				SendMessage(timeListLDUParallel, LB_RESETCONTENT, 0, 0);
				/*TCHAR* buf = new TCHAR[100];
				GetWindowText(hTextThreads, buf, 100);
				int numberThreads = _ttoi(buf);
				if (numberThreads != 0)
				{*/

					double timein = omp_get_wtime();
					x = MatrixDecomposition::LDU(SLE, n);
					double timeout = omp_get_wtime();
					double dt = timeout - timein;
					stringstream sstime;
					sstime << dt/*clock() / 1000.0*/ << endl;
					string newString = sstime.str();
					USES_CONVERSION;
					LPWSTR pstTime = A2T(newString.c_str());
					SendMessage(timeListLDU, LB_ADDSTRING, 0, (LPARAM)pstTime);

					//timein = timeout = dt = 0;
					timein = omp_get_wtime();
					x3 = MatrixDecomposition::LDUParallel(SLE, n, numberThreads);
					timeout = omp_get_wtime();
					dt = timeout - timein;
					stringstream sstime3;
					sstime3 << dt/*clock() / 1000.0 */ << endl;
					newString = sstime3.str();
					pstTime = A2T(newString.c_str());
					SendMessage(timeListLDUParallel, LB_ADDSTRING, 0, (LPARAM)pstTime);


					//srand(time(0));
					//timein = timeout = dt = 0;
					timein = omp_get_wtime();
					x1 = MatrixDecomposition::GaussianElimination(SLE, n);
					timeout = omp_get_wtime();
					dt = timeout - timein;
					stringstream sstime1;
					sstime1 << dt/* clock() / 1000.0*/ << endl;
					newString = sstime1.str();

					LPWSTR pstTime1 = A2T(newString.c_str());

					SendMessage(timeListGaus, LB_ADDSTRING, 0, (LPARAM)pstTime1);

					timein = omp_get_wtime();
					x2 = MatrixDecomposition::GaussianEliminationParallel(SLE, n, numberThreads);
					timeout = omp_get_wtime();
					dt = timeout - timein;
					stringstream sstime2;
					sstime2 << dt/*clock() / 1000.0 */ << endl;
					string newString1 = sstime2.str();
					pstTime = A2T(newString1.c_str());
					SendMessage(timeListGParallel, LB_ADDSTRING, 0, (LPARAM)pstTime);

					for (int i = 0; i < n; i++)
					{
						stringstream ss;
						ss << floor(x[i] * 100) / 100 << endl;
						string newString = ss.str();
						USES_CONVERSION;
						LPWSTR pst = A2T(newString.c_str());
						SendMessage(hWndListX, LB_ADDSTRING, 0, (LPARAM)pst);

						stringstream ss1;
						ss1 << floor(x1[i] * 100) / 100 << endl;
						newString = ss1.str();
						pst = A2T(newString.c_str());
						SendMessage(hWndListX1, LB_ADDSTRING, 0, (LPARAM)pst);
						
						stringstream ss2;
						ss2 << floor(x2[i] * 100) / 100 << endl;
						newString = ss2.str();
						pst = A2T(newString.c_str());
						SendMessage(hWndListX3, LB_ADDSTRING, 0, (LPARAM)pst);
						
						stringstream ss3;
						ss3 << floor(x3[i] * 100) / 100 << endl;
						newString = ss3.str();
						pst = A2T(newString.c_str());
						SendMessage(hWndListX2, LB_ADDSTRING, 0, (LPARAM)pst);
					}
					for (int i = 0; i < n; i++) delete[] SLE.A[i];
					delete[] SLE.A;
					delete[] SLE.B;
			}
		}
		break;
		/*case 4:
		{
			
		}
		break;*/
		case IDM_ABOUT:
			//DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Обработчик сообщений для окна "О программе".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

bool CheckArray(SystemOfLinearEquations SLE,int size) 
{
	if (size < 20)
	{
		int count = 0;
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++) {
				if (SLE.A[i][j] == 0)
					count++;

			}
			if (count == size)
				return false;
			count = 0;
		}
		//return true;
	}
	
	return true;
}