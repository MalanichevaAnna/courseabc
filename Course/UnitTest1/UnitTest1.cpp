﻿#include "pch.h"
#include "CppUnitTest.h"
//#include "../Course/MatrixDecomposition.cpp"
//#include "../Course/SystemOfLinearEquations.cpp"
//#include "../Course/SystemOfLinearEquations.h"
#include "../Course/Course.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		SystemOfLinearEquations SLE1;
		TEST_METHOD(TestMethodLDU_TRUE)
		{
			int size = 3;
		    //SystemOfLinearEquations SLE;
			SLE1.A = new double*[size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];
			
			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 12;
			SLE1.A[1][1] = 3;
			SLE1.A[1][2] = 65;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;


		    MatrixDecomposition matrix;
			
			double* actual = new double[size];
			actual = MatrixDecomposition::LDU(SLE1, size);
			
			double* rezult = new double[size];

			rezult[0] = 0.067;
			rezult[1] = -0.145;
			rezult[2] = 0.394;
			for (int i = 0; i < size; i++) {
				actual[i] = round(actual[i] * 100) / 100;
				rezult[i] = round(rezult[i] * 100) / 100;
			}
			Assert::AreEqual(actual[2],rezult[2]);
		}

		TEST_METHOD(TestMethodLDU_False)
		{
			int size = 3;
			//SystemOfLinearEquations SLE;
			SLE1.A = new double* [size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];

			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 12;
			SLE1.A[1][1] = 3;
			SLE1.A[1][2] = 65;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;


			MatrixDecomposition matrix;

			double* actual = new double[size];
			actual = MatrixDecomposition::LDU(SLE1, size);

			double* rezult = new double[size];

			rezult[0] = 0.067;
			rezult[1] = -0.145;
			rezult[2] = 0.594;
			for (int i = 0; i < size; i++) {
				actual[i] = round(actual[i] * 100) / 100;
				rezult[i] = round(rezult[i] * 100) / 100;
			}
			Assert::AreEqual(actual[2], rezult[2]);
		}

		TEST_METHOD(TestMethodGaussianElimination_TRUE)
		{
			int size = 3;
			//SystemOfLinearEquations SLE;
			SLE1.A = new double* [size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];

			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 12;
			SLE1.A[1][1] = 3;
			SLE1.A[1][2] = 65;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;


			MatrixDecomposition matrix;

			double* actual = new double[size];
			actual = MatrixDecomposition::GaussianElimination(SLE1, size);

			double* rezult = new double[size];

			rezult[0] = 0.067;
			rezult[1] = -0.145;
			rezult[2] = 0.394;
			for (int i = 0; i < size; i++) {
				actual[i] = round(actual[i] * 100) / 100;
				rezult[i] = round(rezult[i] * 100) / 100;
			}
			Assert::AreEqual(actual[2], rezult[2]);
		}

		TEST_METHOD(TestMethodGaussianElimination_False)
		{
			int size = 3;
			//SystemOfLinearEquations SLE;
			SLE1.A = new double* [size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];

			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 12;
			SLE1.A[1][1] = 3;
			SLE1.A[1][2] = 65;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;


			MatrixDecomposition matrix;

			double* actual = new double[size];
			actual = MatrixDecomposition::GaussianElimination(SLE1, size);

			double* rezult = new double[size];

			rezult[0] = 0.067;
			rezult[1] = -0.945;
			rezult[2] = 1.394;
			for (int i = 0; i < size; i++) {
				actual[i] = round(actual[i] * 100) / 100;
				rezult[i] = round(rezult[i] * 100) / 100;
			}
			Assert::AreNotEqual(actual[2], rezult[2]);
		}

		TEST_METHOD(TestMethodLDUParallel_TRUE)
		{
			int size = 3;
			//SystemOfLinearEquations SLE;
			SLE1.A = new double* [size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];

			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 12;
			SLE1.A[1][1] = 3;
			SLE1.A[1][2] = 65;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;


			MatrixDecomposition matrix;

			double* actual = new double[size];
			actual = MatrixDecomposition::LDUParallel(SLE1, size,3);

			double* rezult = new double[size];

			rezult[0] = 0.067;
			rezult[1] = -0.145;
			rezult[2] = 0.394;
			for (int i = 0; i < size; i++) {
				actual[i] = round(actual[i] * 100) / 100;
				rezult[i] = round(rezult[i] * 100) / 100;
			}
			Assert::AreEqual(actual[2], rezult[2]);
		}

		TEST_METHOD(TestMethodLDUParallel_False)
		{
			int size = 3;
			//SystemOfLinearEquations SLE;
			SLE1.A = new double* [size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];

			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 12;
			SLE1.A[1][1] = 3;
			SLE1.A[1][2] = 65;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;


			MatrixDecomposition matrix;

			double* actual = new double[size];
			actual = MatrixDecomposition::LDUParallel(SLE1, size,3);

			double* rezult = new double[size];

			rezult[0] = 0.067;
			rezult[1] = -0.145;
			rezult[2] = 5.394;
			for (int i = 0; i < size; i++) {
				actual[i] = round(actual[i] * 100) / 100;
				rezult[i] = round(rezult[i] * 100) / 100;
			}
			Assert::AreNotEqual(actual[2], rezult[2]);
		}

		TEST_METHOD(TestMethodCheckArrayWithLineZero)
		{
			int size = 3;
			//SystemOfLinearEquations SLE;
			SLE1.A = new double* [size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];

			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 0;
			SLE1.A[1][1] = 0;
			SLE1.A[1][2] = 0;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;

			bool check = CheckArray(SLE1, size);
			Assert::AreEqual(false, check);
		}

		TEST_METHOD(TestMethodCheckArrayWithoutLineZero)
		{
			int size = 3;
			//SystemOfLinearEquations SLE;
			SLE1.A = new double* [size];
			for (int i = 0; i < size; i++) SLE1.A[i] = new double[size];
			SLE1.B = new double[size];

			SLE1.A[0][0] = 5;
			SLE1.A[0][1] = 9;
			SLE1.A[0][2] = 5;
			SLE1.B[0] = 1;

			SLE1.A[1][0] = 1;
			SLE1.A[1][1] = 6;
			SLE1.A[1][2] = 8;
			SLE1.B[1] = 26;

			SLE1.A[2][0] = 89;
			SLE1.A[2][1] = 8;
			SLE1.A[2][2] = 8;
			SLE1.B[2] = 8;

			bool check = CheckArray(SLE1, size);
			Assert::AreEqual(true, check);
		}

	};
}
